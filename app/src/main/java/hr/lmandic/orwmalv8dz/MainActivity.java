package hr.lmandic.orwmalv8dz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private EditText etBrand;
    private Button btnSearch;
    private Call<List<Product>> apiCall;
    RecyclerAdapter adapter;
    RecyclerView rvProducts;
    String sBrand = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etBrand  = (EditText) findViewById(R.id.editText);
        btnSearch = (Button) findViewById(R.id.btnSearch);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sBrand = etBrand.getText().toString();
                setUpRecycler();
                setUpApiCall(sBrand);
                etBrand.setText("");
            }
        });


    }

    private void setUpRecycler() {

        rvProducts = (RecyclerView) findViewById(R.id.rvProducts);
        // Create adapter passing in the sample user data
         adapter = new RecyclerAdapter();
        // Attach the adapter to the recyclerview to populate items
        rvProducts.setLayoutManager(new LinearLayoutManager(this));
        rvProducts.setAdapter(adapter);



    }

    private void setUpApiCall(String sBrand) {

        apiCall = NetworkUtils.getApiInterface().getProducts(sBrand);
        apiCall.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>>
                    response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.i("Laura","successful");
                    setData(response.body());//show
                }
            }
            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData(List<Product> products){

        adapter.addData(products);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (apiCall != null)
            apiCall.cancel();
    }

}

