        package hr.lmandic.orwmalv8dz;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;


public class ProductColor {

    @SerializedName("hex_value")
    @Expose
    private String hexValue;
    @SerializedName("colour_name")
    @Expose
    private String colourName;

    public String getHexValue() {
        return hexValue;
    }

    public void setHexValue(String hexValue) {
        this.hexValue = hexValue;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    @Override
    public String toString() {
        return "ne";
        //return new StringBuilder(this).append("hexValue", hexValue).append("colourName", colourName).toString();
    }

}