package hr.lmandic.orwmalv8dz;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;

public class ProductViewHolder extends RecyclerView.ViewHolder{

    private final ImageView image;
    private final TextView name;
    private final TextView price;
    private final TextView rating;
    private final TextView description;

    public ProductViewHolder(@NonNull View itemView) {

        super(itemView);

        image = itemView.findViewById(R.id.ivProduct);
        name = itemView.findViewById(R.id.tvName);
        price = itemView.findViewById(R.id.tvPrice);
        rating = itemView.findViewById(R.id.tvRating);
        description = itemView.findViewById(R.id.tvDescription);

    }

    public void setupProduct(Product product){

        Picasso.get().load(product.getImageLink()).into(image);
        name.setText("Name: "+ product.getName());
        price.setText( "Price: "+ product.getPrice());
        rating.setText("Rating: "+ product.getRating());
        description.setText(product.getDescription() );

    }

}