package hr.lmandic.orwmalv8dz;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private final List<Product> productList = new ArrayList<>();


    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);


        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {

        holder.setupProduct(productList.get(position));

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void addData(List<Product> productList){

        this.productList.clear();
        this.productList.addAll(productList);
        notifyDataSetChanged();
    }



}
